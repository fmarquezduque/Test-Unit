def ordenarSalarios(nombres,salarios):
    
    for i in range(len(salarios)):
        for j in range(len(salarios)):
            if(salarios[i]>=salarios[j]):
                salarios[i],salarios[j] = salarios[j],salarios[i]
                nombres[i],nombres[j] = nombres[j],nombres[i]
    return nombres

def salarios(nombres,dinero):

    if(len(nombres)==len(dinero)):
        nombres = ordenarSalarios(nombres,dinero)
        return nombres
    else:
       return False


############# test #############

def test_salariosNoOrdenados():
    nombres = salarios(['Juan', 'Pello', 'Ana', 'Ana'],[100,222,5454,5511])
    esperado = ['Juan', 'Pello', 'Ana', 'Ana']
    assert nombres != esperado

def test_salariosOrdenados():
    nombres = salarios(['Juan', 'Pello', 'Ana', 'Ana'],[100,222,5454,5511])
    esperado =  ['Ana', 'Ana', 'Pello', 'Juan']
    assert nombres == esperado

def test_tamainosdiferentes():

    nombres = salarios(['Juan', 'Pello', 'Ana'],[100,222,5454,5511])
    esperado = False
    assert nombres == esperado


################################